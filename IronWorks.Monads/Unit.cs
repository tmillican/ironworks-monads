namespace IronWorks.Monads
{
    using System;

    /// <summary>
    ///   A type that has only one value.
    /// </summary>
    /// <remarks>
    ///   <para>
    ///     <see cref="Unit"/> is not a monad itself, but it is used by monads.
    ///   </para>
    /// </remarks>
    public struct Unit : IEquatable<Unit>
    {
        private  static readonly string _toStringValue = "()";

        /// <summary>
        ///   See <see cref="IEquatable{T}.Equals(T)"/>.
        /// </summary>
        public bool Equals(Unit other)
        {
            return true;
        }

        /// <summary>
        ///   See <see cref="Object.Equals(object)"/>.
        /// </summary>
        public override bool Equals(object other)
        {
            return (other is Unit)
                ? true
                : false;
        }

        /// <summary>
        ///   See <see cref="Object.GetHashCode()"/>.
        /// </summary>
        public override int GetHashCode()
        {
            // Randomly chosen non-zero value
            return -1807512216;
        }

        /// <summary>
        ///   See <see cref="Object.ToString()"/>.
        /// </summary>
        public override string ToString()
        {
            return _toStringValue;
        }
    }
}
