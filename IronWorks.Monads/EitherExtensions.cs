namespace IronWorks.Monads
{
    using System;

    /// <summary>
    ///   Extensions for the <see cref="Either{L, R}"/> class.
    /// </summary>
    public static class EitherExtensions
    {
        /// <summary>
        ///   Produces a left-sided <see cref="Either{L, R}"/> instance from the
        ///   provided value.
        /// </summary>
        public static Either<L, R> ToEitherLeft<L, R>(this L value)
        {
            return new Either<L, R>(value, false);
        }

        /// <summary>
        ///   Produces a right-sided <see cref="Either{L, R}"/> instance from
        ///   the provided value.
        /// </summary>
        public static Either<L, R> ToEitherRight<L, R>(this R value)
        {
            return new Either<L, R>(value);
        }

        /// <summary>
        ///   LINQ wrapper for the <see cref="Either{L, R}.Bind{RResult}(Func{R,
        ///   Either{L, RResult}})"/> method.
        /// </summary>
        /// <typeparam name="L">
        ///   The left type of the <paramref name="source"/> <see
        ///   cref="Either{L,R}"/> instance.
        /// </typeparam>
        /// <typeparam name="R">
        ///   The right type of the <paramref name="source"/> <see
        ///   cref="Either{L,R}"/> instance.
        /// </typeparam>
        /// <typeparam name="RResult">
        ///   The right type of the result <see cref="Either{L,R}"/> instance.
        /// </typeparam>
        /// <param name="source">
        ///   The source <see cref="Either{L, R}"/> instance.
        /// </param>
        /// <param name="selector">
        ///   A delegate that takes an argument of type <typeparamref name="R"/>
        ///   and returns an <code>Either&lt;L, RResult&gt;</code> instance.
        /// </param>
        /// <throws cref="ArgumentNullException">
        ///   <paramref name="selector"/> is <code>null</code>.
        /// </throws>
        /// <returns>
        ///   An instance of type <code>Either&lt;L, RResult&gt;</code> produced
        ///   by binding the <paramref name="source"/> argument with the
        ///   <paramref name="selector"/> delegate.
        /// </returns>
        public static Either<L, RResult> Select<L, R, RResult>(
            this Either<L, R> source,
            Func<R, Either<L, RResult>> selector)
        {
            if (selector is null)
            {
                throw new ArgumentNullException(nameof(selector));
            }

            return source.Bind(selector);
        }

        /// <summary>
        ///   LINQ wrapper for the <see cref="Either{L, R}.Bind{RResult}(Func{R,
        ///   Either{L, RResult}})"/> method.
        /// </summary>
        /// <typeparam name="L">
        ///   The left type of the <paramref name="source"/> <see
        ///   cref="Either{L,R}"/> instance.
        /// </typeparam>
        /// <typeparam name="R">
        ///   The right type of the <paramref name="source"/> <see
        ///   cref="Either{L,R}"/> instance.
        /// </typeparam>
        /// <typeparam name="RResult">
        ///   The right type of the result <see cref="Either{L,R}"/> instance.
        /// </typeparam>
        /// <param name="source">
        ///   The source <see cref="Either{L, R}"/> instance.
        /// </param>
        /// <param name="selector">
        ///   A delegate that takes an argument of type <typeparamref name="R"/>
        ///   and returns an <code>Either&lt;L, RResult&gt;</code> instance.
        /// </param>
        /// <throws cref="ArgumentNullException">
        ///   <paramref name="source"/> or <paramref name="selector"/>
        ///   is <code>null</code>.
        /// </throws>
        /// <returns>
        ///   An instance of type <code>Either&lt;L, RResult&gt;</code> produced
        ///   by binding the <paramref name="source"/> argument with the
        ///   <paramref name="selector"/> delegate.
        /// </returns>
        public static Either<L, RResult> SelectMany<L, R, RResult>(
            this Either<L, R> source,
            Func<R, Either<L, RResult>> selector)
        {
            return Select(source, selector);
        }

        /// <summary>
        ///   LINQ wrapper for the <see cref="Either{L, R}.Bind{RResult}(Func{R,
        ///   Either{L, RResult}})"/> method used in nested "from..." queries.
        /// </summary>
        /// <typeparam name="L">
        ///   The left type of the <paramref name="source"/> <see
        ///   cref="Either{L,R}"/> instance.
        /// </typeparam>
        /// <typeparam name="R">
        ///   The right type of the <paramref name="source"/> <see
        ///   cref="Either{L,R}"/> instance.
        /// </typeparam>
        /// <typeparam name="RInner">
        ///   The right type of the inner <see cref="Either{L,R}"/> instance.
        /// </typeparam>
        /// <typeparam name="RResult">
        ///   The right type of the result <see cref="Either{L,R}"/> instance.
        /// </typeparam>
        /// <param name="source">
        ///   The source <see cref="Either{L, R}"/> instance.
        /// </param>
        /// <param name="innerSelector">
        ///   A delegate that takes an argument of type <typeparamref name="R"/>
        ///   and returns an <code>Either&lt;L, RInner&gt;</code> instance.
        /// </param>
        /// <param name="resultSelector">
        ///   A delegate that takes arguments of type <typeparamref name="R"/>
        ///   and <typeparamref name="RInner"/>, and returns a value of type
        ///   <typeparamref name="RResult"/>.
        /// </param>
        /// <throws cref="ArgumentNullException">
        ///   <paramref name="innerSelector"/> or <paramref
        ///   name="resultSelector"/> is <code>null</code>.
        /// </throws>
        /// <returns>
        ///   An instance of type <code>Either&lt;L, RResult&gt;</code> produced
        ///   by nested binding of the <paramref name="source"/> argument with
        ///   the <paramref name="resultSelector"/> and <paramref
        ///   name="innerSelector"/> delegates.
        /// </returns>
        public static Either<L, RResult> SelectMany<L, R, RInner, RResult>(
            this Either<L, R> source,
            Func<R, Either<L, RInner>> innerSelector,
            Func<R, RInner, RResult> resultSelector)
        {
            if (innerSelector is null)
            {
                throw new ArgumentNullException(nameof(innerSelector));
            }
            if (resultSelector is null)
            {
                throw new ArgumentNullException(nameof(resultSelector));
            }

            return source.Match(
                left: lv => new Either<L, RResult>(lv, false),
                right: rv => source.Bind(innerSelector).Match(
                    left: ilv => new Either<L, RResult>(ilv, false),
                    right: irv => new Either<L, RResult>(
                        resultSelector(rv, irv))));
        }
    }
}
