namespace IronWorks.Monads
{
    using System;

    /// <summary>
    ///   A struct representing either a value of type <typeparamref name="T"/>
    ///   or a value of type <see cref="None{T}"/>.
    /// </summary>
    /// <typeparam name="T">
    ///   The underlying type of the <see cref="Option{T}"/>.
    /// </typeparam>
    public struct Option<T>
    {
        // Properties
        // =====================================================================

        /// <summary>
        ///   Gets a value indicating whether the this <see cref="Option{T}"/>
        ///   instance contains a value of type <see cref="None{T}"/>.
        /// </summary>
        /// <returns>
        ///   If this <see cref="Option{T}"/> instance contains a value of type
        ///   <see cref="None{T}"/>, <code>true</code>. Otherwise,
        ///   <code>false</code>.
        /// </returns>
        public bool IsNone { get { return _either.IsLeft; } }

        /// <summary>
        ///   Gets a value indicating whether the this <see cref="Option{T}"/>
        ///   instance contains a value of type <typeparamref name="T"/>.
        /// </summary>
        /// <returns>
        ///   If this <see cref="Option{T}"/> instance contains a value of type
        ///   <typeparamref name="T"/>, <code>true</code>. Otherwise,
        ///   <code>false</code>.
        /// </returns>
        public bool IsSome { get { return _either.IsRight; } }

        // Fields
        // =====================================================================

        private Either<None<T>, T> _either;

        // Ctors
        // =====================================================================

        /// <summary>
        ///   Initializes a new instance of the <see cref="Option{T}"/> class
        ///   with the provided value of type <typeparamref name="T"/>.
        /// </summary>
        /// <param name="value">
        ///   The internal value of type <typeparamref name="T"/> that the <see
        ///   cref="Option{T}"/> value will contain.
        /// </param>
        public Option(T value)
        {
            _either = new Either<None<T>, T>(value);
        }

        /// <summary>
        ///   Initializes a new instance of the <see cref="Option{T}"/> class
        ///   with a value of <see cref="None{T}"/>.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     This constructor's signature is a kludge to prevent conflicting
        ///     signatures regardless of what type <typeparamref name="T"/>
        ///     unifies to. For cleaner syntax sugar, you may prefer to use the
        ///     static <see cref="Option{T}.Some(T)"/>, <see
        ///     cref="Option{T}.Identity(T)"/> and <see
        ///     cref="Option{T}.None()"/> methods.
        ///   </para>
        /// </remarks>
        /// <param name="ignore1">
        ///   This parameter is ignored. It exists to give this constructor a
        ///   distinct signature from <see cref="Option(T)"/> no matter what
        ///   type <typeparamref name="T"/> unifies to.
        /// </param>
        /// <param name="ignore2">
        ///   This parameter is ignored. It exists to give this constructor a
        ///   distinct signature from <see cref="Option(T)"/> no matter what
        ///   type <typeparamref name="T"/> unifies to.
        /// </param>
        public Option(bool ignore1, bool ignore2)
        {
            _either = new Either<None<T>, T>(new None<T>(), false);
        }

        /// <summary>
        ///   Binds this <see cref="Option{T}"/> instance into an instance of
        ///   type <code>Option&lt;TResult&gt;</code> using the provided
        ///   delegate.
        /// </summary>
        /// <param name="bindFunc">
        ///   A delegate that takes an argument of type <typeparamref name="T"/>
        ///   and returns a vlue of type <code>Option&lt;TResult&gt;</code>.
        /// </param>
        /// <throws cref="ArgumentNullException">
        ///   <paramref name="bindFunc"/> is <code>null</code>.
        /// </throws>
        /// <returns>
        ///   If <see cref="IsSome"/> is <code>true</code>, an instance of type
        ///   <code>Option&lt;TResult&gt;</code> produced by applying <paramref
        ///   name="bindFunc"/> to the internal value of this <see
        ///   cref="Option{T}"/>. Otherwise, an instance of of type
        ///   <code>Option&lt;TResult&gt;</code> containing
        ///   <code>None&lt;TResult&gt;</code>.
        /// </returns>
        public Option<TResult> Bind<TResult>(Func<T, Option<TResult>> bindFunc)
        {
            if (bindFunc is null)
            {
                throw new ArgumentNullException(nameof(bindFunc));
            }

            return _either.Match(
                left: lv => new Option<TResult>(),
                right: rv => bindFunc(rv));
        }

        /// <summary>
        ///   See <see cref="Object.Equals(object)"/>.
        /// </summary>
        public override bool Equals(object other)
        {
            if (other is Option<T>)
            {
                return Equals((Option<T>)other);
            }
            return false;
        }

        /// <summary>
        ///   See <see cref="IEquatable{T}.Equals(T)"/>.
        /// </summary>
        public bool Equals(Option<T> other)
        {
            return _either.Equals(other._either);
        }

        /// <summary>
        ///   Returns a value indicating whether this <see cref="Option{T}"/>
        ///   has some value (see <see cref="IsSome"/>), and that value is
        ///   equivlanet equivalent to <paramref name="other"/>.
        /// </summary>
        /// <param name="other">
        ///   The value to compare to.
        /// </param>
        /// <returns>
        ///   <code>true</code> if <see cref="IsSome"/> is <code>true</code>
        ///   and the value of this <see cref="Option{T}"/> is equivalent to
        ///   <paramref name="other"/>. Otherwise, <code>false</code>.
        /// </returns>
        public bool EqualsSome(T other)
        {
            return _either.EqualsRight(other);
        }

        /// <summary>
        ///   Returns a value indicating whether this <see cref="Option{T}"/>
        ///   has "none" value (see <see cref="IsNone"/>), and that value is
        ///   equivalent to <paramref name="other"/>.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     This method is largely useless as <see cref="None{T}"/> is
        ///     a unit value for any given <typeparamref name="T"/>. It is
        ///     provided for completion's sake.
        ///   </para>
        /// </remarks>
        /// <param name="other">
        ///   The value to compare to.
        /// </param>
        /// <returns>
        ///   <code>true</code> if <see cref="IsSome"/> is <code>true</code>
        ///   and the value of this <see cref="Option{T}"/> is equivalent to
        ///   <paramref name="other"/>. Otherwise, <code>false</code>.
        /// </returns>
        public bool EqualsNone(None<T> other)
        {
            return IsNone;
        }

        /// <summary>
        ///   See <see cref="Object.GetHashCode()"/>.
        /// </summary>
        public override int GetHashCode()
        {
            return _either.GetHashCode();
        }

        /// <summary>
        ///   The identity function of <see cref="Option{T}"/>.
        /// </summary>
        /// <param name="value">
        ///   The internal value of the <see cref="Option{T}"/>.
        /// </param>
        /// <returns>
        ///   An <see cref="Option{T}"/> instance containing <paramref
        ///   name="value"/>.
        /// </returns>
        public static Option<T> Identity(T value)
        {
            return new Option<T>(value);
        }

        /// <summary>
        ///   Produces a value of type <typeparamref name="TResult"/> from this
        ///   <see cref="Option{T}"/> instance using one of the provided
        ///   delegates.
        /// </summary>
        /// <typeparam name="TResult">
        ///   The type of the result.
        /// </typeparam>
        /// <param name="none">
        ///   A delegate that takes an argument of type <see cref="None{T}"/>
        ///   and returns a value of type <typeparamref name="TResult"/>.
        /// </param>
        /// <param name="some">
        ///   A delegate that takes an argument of type <typeparamref name="T"/>
        ///   and returns a value of type <typeparamref name="TResult"/>.
        /// </param>
        /// <throws cref="ArgumentNullException">
        ///   <paramref name="none"/> is <code>null</code>.
        /// </throws>
        /// <throws cref="ArgumentNullException">
        ///   <paramref name="some"/> is <code>null</code>.
        /// </throws>
        /// <returns>
        ///   If <see cref="IsSome"/> is <code>true</code>, a value of type
        ///   <typeparamref name="TResult"/> produced by applying <paramref
        ///   name="some"/> to the internal value of this <see
        ///   cref="Option{T}"/>. Otherwise, a value of type <typeparamref
        ///   name="TResult"/> produced by applying <paramref name="none"/> to
        ///   the <see cref="None{T}"/> value.
        /// </returns>
        public TResult Match<TResult>(
            Func<None<T>,
            TResult> none,
            Func<T, TResult> some)
        {
            if (none is null)
            {
                throw new ArgumentNullException(nameof(none));
            }

            if (some is null)
            {
                throw new ArgumentNullException(nameof(some));
            }

            return _either.Match(
                left: lv => none(lv),
                right: rv => some(rv));
        }

        /// <summary>
        ///   Produces an <see cref="Option{T}"/> instance containing <see
        ///   cref="None{T}"/>.
        /// </summary>
        /// <returns>
        ///   An <see cref="Option{T}"/> instance containing <see
        ///   cref="None{T}"/>.
        /// </returns>
        public static Option<T> None()
        {
            return new Option<T>();
        }

        /// <summary>
        ///   Produces an <see cref="Option{T}"/> instance containing the
        ///   specified <paramref name="value"/>.
        /// </summary>
        /// <param name="value">
        ///   The internal value of the <see cref="Option{T}"/>.
        /// </param>
        /// <returns>
        ///   An <see cref="Option{T}"/> instance containing <paramref
        ///   name="value"/>.
        /// </returns>
        public static Option<T> Some(T value)
        {
            return new Option<T>(value);
        }

        /// <summary>
        ///   See <see cref="Object.ToString()"/>.
        /// </summary>
        public override string ToString()
        {
            return _either.ToString();
        }
    }
}
