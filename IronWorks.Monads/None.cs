namespace IronWorks.Monads
{
    using System;

    /// <summary>
    ///   A monad representing none of some type <typeparamref name="T"/>.
    /// </summary>
    /// <remarks>
    ///   <para>
    ///     This is similar to <see cref="Unit"/> in that for each <typeparamref
    ///     name="T"/> there is only a single possible <see cref="None{T}"/>
    ///     value. However, unlike <see cref="Unit"/>, <see cref="None{T}"/> is
    ///     a monad and can be bound to a None&lt;T&gt; of some other underlying
    ///     type.
    ///   </para>
    ///   <para>
    ///     Note that <see cref="None{T}"/> values of different underlying types
    ///     are *not* considered equal. That is, having no bananas is different
    ///     from having no apples, though one can convert between the two via
    ///     Bind()/SelectMany().
    ///   </para>
    /// </remarks>
    public struct None<T> : IEquatable<None<T>>
    {
        // Fields
        // =====================================================================

        private static readonly string _toStringValue =
            $"None<{typeof(T).FullName}>";

        // Ctors
        // =====================================================================

        /// <summary>
        ///   Construct a new <see cref="None{T}"/> instance.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Please note that the underlying value of a <see cref="None{T}"/>
        ///     is always implicitly <code>default(T)</code> regardless of
        ///     <paramref name="value"/>. The <paramref name="value"/> argument
        ///     exists strictly to satisfy the monadic properties.
        ///   </para>
        ///   <para>
        ///     This method is provided for symmetry with the <see
        ///     cref="Identity(T)"/> method. There is no reason to use this over
        ///     the implict default constructor.
        ///   </para>
        /// </remarks>
        public None(T value) {}

        // Methods
        // =====================================================================

        /// <summary>
        ///   Produces a value of type <code>None&lt;TResult&gt;</code> using
        ///   the provided <paramref name="bindFunc"/> delegate.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Note that the underlying value of type <typeparamref name="T"/>
        ///     encapsulated by <see cref="None{T}"/> is always implicitly
        ///     <code>default(T)</code>, regardless of what value was given
        ///     to the <see cref="Identity"/> function.
        ///   </para>
        /// </remarks>
        /// <typeparam name="TResult">
        ///   The underlying type of the result.
        /// </typeparam>
        /// <param name="bindFunc">
        ///   The delegate function to apply when binding this value.
        /// </param>
        /// <throws cref="ArgumentNullException">
        ///   <paramref name="bindFunc"/> is <code>null</code>.
        /// </throws>
        /// <returns>
        ///   A value of type <code>None&lt;TResult&gt;</code> produced by
        ///   applying <paramref name="bindFunc"/> to <code>default(T)</code>.
        /// </returns>
        public None<TResult> Bind<TResult>(Func<T, None<TResult>> bindFunc)
        {
            return (!(bindFunc is null))
                ? bindFunc(default(T))
                : new None<TResult>();
        }

        /// <summary>
        ///   See <see cref="IEquatable{T}.Equals(T)"/>.
        /// </summary>
        public bool Equals(None<T> other)
        {
            return true;
        }

        /// <summary>
        ///   See <see cref="Object.Equals(object)"/>.
        /// </summary>
        public override bool Equals(object other)
        {
            return (other is None<T>)
                ? true
                : false;
        }

        /// <summary>
        ///   See <see cref="Object.GetHashCode()"/>.
        /// </summary>
        public override int GetHashCode()
        {
            return (typeof(T).GetHashCode());
        }

        /// <summary>
        ///   The identity function of <see cref="None{T}"/>.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Please note that the underlying value of a <see cref="None{T}"/>
        ///     is always implicitly <code>default(T)</code> regardless of
        ///     <paramref name="value"/>. The <paramref name="value"/> argument
        ///     exists strictly to satisfy the monadic properties.
        ///   </para>
        /// </remarks>
        /// <returns>
        ///   The <see cref="None{T}"/> value for type <typeparamref name="T"/>.
        /// </returns>
        public static None<T> Identity(T value)
        {
            return new None<T>();
        }

        /// <summary>
        ///   See <see cref="Object.ToString()"/>.
        /// </summary>
        public override string ToString()
        {
            return _toStringValue;
        }
    }
}
