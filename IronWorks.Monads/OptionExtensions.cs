namespace IronWorks.Monads
{
    using System;

    /// <summary>
    ///   Extensions for the <see cref="Option{T}"/> class.
    /// </summary>
    public static class OptionExtensions
    {
        /// <summary>
        ///   Produces a none-valued <see cref="Option{T}"/> instance from the
        ///   provided value's type.
        /// </summary>
        public static Option<T> ToOptionNone<T>(this T value)
        {
            return new Option<T>(false, false);
        }

        /// <summary>
        ///   Produces a some-valued <see cref="Option{T}"/> instance from
        ///   the provided value.
        /// </summary>
        public static Option<T> ToOptionSome<T>(this T value)
        {
            return new Option<T>(value);
        }

        /// <summary>
        ///   LINQ wrapper for the <see cref="Option{T}.Bind{TResult}(Func{T,
        ///   Option{TResult}})"/> method.
        /// </summary>
        /// <typeparam name="T">
        ///   The underlying type of the <paramref name="source"/> <see
        ///   cref="Option{T}"/> instance.
        /// </typeparam>
        /// <typeparam name="TResult">
        ///   The underlying type of the result <see cref="Option{T}"/> instance.
        /// </typeparam>
        /// <param name="source">
        ///   The source <see cref="Option{T}"/> instance.
        /// </param>
        /// <param name="selector">
        ///   A delegate that takes an argument of type <typeparamref name="T"/>
        ///   and returns an <code>Option&lt;TResult&gt;</code> instance.
        /// </param>
        /// <throws cref="ArgumentNullException">
        ///   <paramref name="selector"/> is <code>null</code>.
        /// </throws>
        /// <returns>
        ///   An instance of type <code>Option&lt;TResult&gt;</code> produced
        ///   by binding the <paramref name="source"/> argument with the
        ///   <paramref name="selector"/> delegate.
        /// </returns>
        public static Option<TResult> Select<T, TResult>(
            this Option<T> source,
            Func<T, Option<TResult>> selector)
        {
            if (selector is null)
            {
                throw new ArgumentNullException(nameof(selector));
            }

            return source.Bind(selector);
        }

        /// <summary>
        ///   LINQ wrapper for the <see cref="Option{T}.Bind{TResult}(Func{T,
        ///   Option{TResult}})"/> method.
        /// </summary>
        /// <typeparam name="T">
        ///   The underlying type of the <paramref name="source"/> <see
        ///   cref="Option{T}"/> instance.
        /// </typeparam>
        /// <typeparam name="TResult">
        ///   The underlying type of the result <see cref="Option{T}"/>
        ///   instance.
        /// </typeparam>
        /// <param name="source">
        ///   The source <see cref="Option{T}"/> instance.
        /// </param>
        /// <param name="selector">
        ///   A delegate that takes an argument of type <typeparamref name="T"/>
        ///   and returns an <code>Option&lt;TResult&gt;</code> instance.
        /// </param>
        /// <throws cref="ArgumentNullException">
        ///   <paramref name="source"/> or <paramref name="selector"/>
        ///   is <code>null</code>.
        /// </throws>
        /// <returns>
        ///   An instance of type <code>Option&lt;TResult&gt;</code> produced
        ///   by binding the <paramref name="source"/> argument with the
        ///   <paramref name="selector"/> delegate.
        /// </returns>
        public static Option<TResult> SelectMany<T, TResult>(
            this Option<T> source,
            Func<T, Option<TResult>> selector)
        {
            return Select(source, selector);
        }

        /// <summary>
        ///   LINQ wrapper for the <see cref="Option{T}.Bind{TResult}(Func{T,
        ///   Option{TResult}})"/> method used in nested "from..." queries.
        /// </summary>
        /// <typeparam name="T">
        ///   The underlying type of the <paramref name="source"/> <see
        ///   cref="Option{T}"/> instance.
        /// </typeparam>
        /// <typeparam name="TInner">
        ///   The underlying type of the inner <see cref="Option{T}"/> instance.
        /// </typeparam>
        /// <typeparam name="TResult">
        ///   The underlying type of the result <see cref="Option{T}"/>
        ///   instance.
        /// </typeparam>
        /// <param name="source">
        ///   The source <see cref="Option{T}"/> instance.
        /// </param>
        /// <param name="innerSelector">
        ///   A delegate that takes an argument of type <typeparamref name="T"/>
        ///   and returns an <code>Option&lt;TInner&gt;</code> instance.
        /// </param>
        /// <param name="resultSelector">
        ///   A delegate that takes arguments of type <typeparamref name="T"/>
        ///   and <typeparamref name="TInner"/>, and returns a value of type
        ///   <typeparamref name="TResult"/>.
        /// </param>
        /// <throws cref="ArgumentNullException">
        ///   <paramref name="innerSelector"/> or <paramref
        ///   name="resultSelector"/> is <code>null</code>.
        /// </throws>
        /// <returns>
        ///   An instance of type <code>Option&lt;TResult&gt;</code> produced
        ///   by nested binding of the <paramref name="source"/> argument with
        ///   the <paramref name="resultSelector"/> and <paramref
        ///   name="innerSelector"/> delegates.
        /// </returns>
        public static Option<TResult> SelectMany<T, TInner, TResult>(
            this Option<T> source,
            Func<T, Option<TInner>> innerSelector,
            Func<T, TInner, TResult> resultSelector)
        {
            if (innerSelector is null)
            {
                throw new ArgumentNullException(nameof(innerSelector));
            }
            if (resultSelector is null)
            {
                throw new ArgumentNullException(nameof(resultSelector));
            }

            return source.Match(
                none: nv => new Option<TResult>(false, false),
                some: sv => source.Bind(innerSelector).Match(
                    none: inv => new Option<TResult>(false, false),
                    some: isv => new Option<TResult>(
                        resultSelector(sv, isv))));
        }
    }
}
