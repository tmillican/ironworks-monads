namespace IronWorks.Monads
{
    using System;

    /// <summary>
    ///   A monad representing the disjoint union of two types. That is, a "sum
    ///   type" or "discriminated union".
    /// </summary>
    /// <remarks>
    ///   <para>
    ///     Please be aware that the implicit default constructor for <see
    ///     cref="Either{L, R}"/> creates a left-sided <see cref="Either{L,
    ///     R}"/> with an internal value of <code>default(L)</code>, and that
    ///     <see cref="Either{L, R}"/> structs are immutable. Keep this in mind
    ///     when declaring arrays of <see cref="Either{L, R}"/> structs.
    ///   </para>
    ///   <para>
    ///     As well, <see cref="Either{L, R}"/> contains both a member of type
    ///     <typeparamref name="L"/> and a member of type <typeparamref
    ///     name="R"/>. Be aware that if <typeparamref name="L"/> and
    ///     <typeparamref name="R"/> are both value types, it is possible for
    ///     <see cref="Either{L, R}"/> to be larger than the recommended 16-byte
    ///     limit.
    ///   </para>
    ///   <para>
    ///     In defense of the above design decision, structs have the advantage
    ///     of being non-nullable, which is a very desirable property for
    ///     monads. However, C# structs cannot inherit (except implicitly from
    ///     object). Thus there is no way to split into separate left and right
    ///     implementations that only contain a single value. A common "IEither"
    ///     interface is another possibility, but interface references are also
    ///     nullable, putting us back in square one. To me, the lesser of three
    ///     evils is to use a single struct with a "caveat emptor" that <see
    ///     cref="Either{L, R}"/> types with two large value types may be
    ///     unsuitable for use in performance-critical sections.
    ///   </para>
    /// </remarks>
    /// <typeparamref name="L">
    ///   The left type for this <see cref="Either{L, R}"/>.
    /// </typeparamref>
    /// <typeparamref name="R">
    ///   The right type for this <see cref="Either{L, R}"/>.
    /// </typeparamref>
    public struct Either<L, R> : IEquatable<Either<L, R>>
    {
        // Properties
        // =====================================================================

        /// <summary>
        ///   Gets a value indicating whether the value of this <see
        ///   cref="Either{L, R}"/> originated from the left set.
        /// </summary>
        /// <returns>
        ///   If the value of this <see cref="Either{L, R}"/> originated from
        ///   the left set, <code>true</code>. Otherwise <code>false</code>.
        /// </returns>
        public bool IsLeft { get { return !_isRight; } }

        /// <summary>
        ///   Gets a value indicating whether the value of this <see
        ///   cref="Either{L, R}"/> originated from the right set.
        /// </summary>
        /// <returns>
        ///   If the value of this <see cref="Either{L, R}"/> originated from
        ///   the right set, <code>true</code>. Otherwise <code>false</code>.
        /// </returns>
        public bool IsRight { get { return _isRight; } }

        // Fields
        // =====================================================================

        private readonly bool _isRight;

        private readonly L _leftValue;

        private readonly R _rightValue;

        // Ctors
        // =====================================================================

        /// <summary>
        ///   Initializes a new value of the <see cref="Either{L, R}"/> struct
        ///   as a right-handed either monad with the specified right value.
        /// </summary>
        /// <param name="rightValue">
        ///   The right side value of the <see cref="Either{L, R}"/>.
        /// </param>
        public Either(R rightValue)
        {
            _isRight = true;
            _leftValue = default(L);
            _rightValue = rightValue;
        }

        /// <summary>
        ///   Initializes a new value of the <see cref="Either{L, R}"/> struct
        ///   as a left-handed either monad with the specified left value.
        /// </summary>
        /// <param name="leftValue">
        ///   The left side value of the <see cref="Either{L, R}"/>.
        /// </param>
        /// <param name="ignored">
        ///   This parameter is ignored. It exists simply to distinguish this
        ///   constructor from the right-sided constructor given that
        ///   <typeparamref name="L"/> and <typeparamref name="R"/> may unify to
        ///   the same type. Equivalently, you may want to use the static <see
        ///   cref="Either{L, R}.Left(L)"/> and <see cref="Either{L,
        ///   R}.Right(R)"/> methods.
        /// </param>
        public Either(L leftValue, bool ignored)
        {
            _isRight = false;
            _leftValue = leftValue;
            _rightValue = default(R);
        }

        // Methods
        // =====================================================================

        /// <summary>
        ///   Produces a value of type <code>Either&lt;L, RResult&gt;</code>
        ///   from this <see cref="Either{L, R}"/> instance.
        /// </summary>
        /// <typeparam name="RResult">
        ///   The right type of the result.
        /// </typeparam>
        /// <param name="bindFunc">
        ///   The delegate function to apply when binding this instance.
        /// </param>
        /// <throws cref="ArgumentNullException">
        ///   <paramref name="bindFunc"/> is <code>null</code>.
        /// </throws>
        /// <returns>
        ///   If <see cref="IsRight"/> is <code>true</code>, an instance of type
        ///   <code>Either&lt;L, RResult</code> created by applying <paramref
        ///   name="bindFunc"/> to the value of this <see cref="Either{L, R}"/>.
        ///   Otherwise, a new value of type <code>Either&lt;L, RResult</code>
        ///   created by left-injection of the value of this <see
        ///   cref="Either{L, R}"/> instance.
        /// </returns>
        public Either<L, RResult> Bind<RResult>(
            Func<R, Either<L, RResult>> bindFunc)
        {
            if (bindFunc is null)
            {
                throw new ArgumentNullException(nameof(bindFunc));
            }

            return IsRight
                ? bindFunc(_rightValue)
                : new Either<L, RResult>(_leftValue, false);
        }

        /// <summary>
        ///   See <see cref="IEquatable{T}.Equals(T)"/>.
        /// </summary>
        public bool Equals(Either<L, R> other)
        {
            if (IsRight)
            {
                // We are right-sided
                return other.IsRight
                    ? EqualsRight(other._rightValue)
                    : false;
            }

            // We are left-sided
            return other.IsRight
                ? false
                : EqualsLeft(other._leftValue);
        }

        /// <summary>
        ///   Returns a value indicating whether this <see cref="Either{L, R}"/>
        ///   is left-sided and has a value equivalent to <paramref
        ///   name="other"/>.
        /// </summary>
        /// <param name="other">
        ///   The value to compare to.
        /// </param>
        /// <returns>
        ///   <code>true</code> if <see cref="IsLeft"/> is <code>true</code>
        ///   and the value of this <see cref="Either{L, R}"/> is equivalent
        ///   to <paramref name="other"/>. Otherwise, <code>false</code>.
        /// </returns>
        public bool EqualsLeft(L other)
        {
            if (IsRight)
            {
                return false;
            }

            // If the left value is valued type or a non-null reference value....
            if (typeof(L).IsValueType || !((_leftValue as object) is null))
            {
                return _leftValue.Equals(other);
            }

            // ...otherwise, this._leftValue is a null reference, equal only to
            // other null references.
            return (other as object) is null;
        }

        /// <summary>
        ///   Returns a value indicating whether this <see cref="Either{L, R}"/>
        ///   is right-sided and has a value equivalent to <paramref
        ///   name="other"/>.
        /// </summary>
        /// <param name="other">
        ///   The value to compare to.
        /// </param>
        /// <returns>
        ///   <code>true</code> if <see cref="IsRight"/> is <code>true</code>
        ///   and the value of this <see cref="Either{L, R}"/> is equivalent
        ///   to <paramref name="other"/>. Otherwise, <code>false</code>.
        /// </returns>
        public bool EqualsRight(R other)
        {
            if (!IsRight)
            {
                return false;
            }

            // If the right value is valued type or a non-null reference value....
            if (typeof(R).IsValueType || !((_rightValue as object) is null))
            {
                return _rightValue.Equals(other);
            }

            // ...otherwise, this._rightValue is a null reference, equal only to
            // other null references.
            return (other as object) is null;
        }

        /// <summary>
        ///   The identity function of <see cref="Either{L, R}"/>.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Equivalent to <see cref="Right(R)"/>, paralleling the right
        ///     handed transformation of <code>Bind(...)</code>.
        ///   </para>
        /// </remarks>
        /// <param name="value">
        ///   The internal value of the <see cref="Either{L, R}"/>.
        /// </param>
        /// <returns>
        ///   A right-sided <see cref="Either{L, R}"/> with an internal value of
        ///   <paramref name="value"/>.
        /// </returns>
        public static Either<L, R> Identity(R value)
        {
            return new Either<L, R>(value);
        }

        /// <summary>
        ///   Produces a left-sided <see cref="Either{L, R}"/> instance.
        /// </summary>
        /// <param name="value">
        ///   The internal value of the <see cref="Either{L, R}"/>.
        /// </param>
        /// <returns>
        ///   A left-sided <see cref="Either{L, R}"/> with an internal value of
        ///   <paramref name="value"/>.
        /// </returns>
        public static Either<L, R> Left(L value)
        {
            return new Either<L, R>(value, false);
        }

        /// <summary>
        ///   Produces a value of type <typeparamref name="TResult"/> from this
        ///   <see cref="Either{L, R}"/> instance.
        /// </summary>
        /// <typeparam name="TResult">
        ///   The type of the result.
        /// </typeparam>
        /// <param name="left">
        ///   A delegate of type <code>Func&lt;L, TRresult&gt;</code> to apply
        ///   when the value of this <see cref="Either{L, R}"/> instance
        ///   originated from the left set.
        /// </param>
        /// <param name="right">
        ///   A delegate of type <code>Func&lt;L, TRresult&gt;</code> to apply
        ///   when the value of this <see cref="Either{L, R}"/> instance
        ///   originated from the right set.
        /// </param>
        /// <throws cref="ArgumentNullException">
        ///   <paramref name="left"/> is <code>null</code>.
        /// </throws>
        /// <throws cref="ArgumentNullException">
        ///   <paramref name="right"/> is <code>null</code>.
        /// </throws>
        /// <returns>
        ///   A value of type <typeparamref name="TResult"/> produced by the
        ///   application of either the <paramref name="left"/> or <paramref
        ///   name="right"/> delegate, according to which set the value of this
        ///   <see cref="Either{L, R}"/> originated from.
        /// </returns>
        public TResult Match<TResult>(
            Func<L, TResult> left, Func<R, TResult> right)
        {
            if (left is null)
            {
                throw new ArgumentNullException(nameof(left));
            }
            if (right is null)
            {
                throw new ArgumentNullException(nameof(right));
            }

            return IsRight
                ? right(_rightValue)
                : left(_leftValue);
        }

        /// <summary>
        ///   Produces a right-sided <see cref="Either{L, R}"/> instance.
        /// </summary>
        /// <param name="value">
        ///   The internal value of the <see cref="Either{L, R}"/>.
        /// </param>
        /// <returns>
        ///   A right-sided <see cref="Either{L, R}"/> with an internal value of
        ///   <paramref name="value"/>.
        /// </returns>
        public static Either<L, R> Right(R value)
        {
            return new Either<L, R>(value);
        }

        /// <summary>
        ///   See <see cref="Object.ToString()"/>.
        /// </summary>
        public override string ToString()
        {
            if (IsRight)
            {
                if (typeof(R).IsValueType
                    || !((_rightValue as object) is null))
                {
                    return _rightValue.ToString();
                }
                return "";
            }

            if (typeof(L).IsValueType
                || !((_leftValue as object) is null))
            {
                return _leftValue.ToString();
            }
            return "";
        }
    }
}
