namespace IronWorks.Monads
{
    using System;

    /// <summary>
    ///   Extensions for the <see cref="None{T}"/> class.
    /// </summary>
    public static class NoneExtensions
    {
        /// <summary>
        ///   Produces a <see cref="None{T}"/> instance for the type of the
        ///   provided value.
        /// </summary>
        public static None<T> ToNone<T>(this T value)
        {
            return new None<T>();
        }

        /// <summary>
        ///   LINQ wrapper for the <see cref="None{T}.Bind{TResult}(Func{T,
        ///   None{TResult}})"/> method.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Unlike the other monads in this library, you can give a
        ///     <code>null</code> selector to <code>Select(None{T}, ...)</code>
        ///     as it is only evaluated for its side-effects.
        ///   </para>
        /// </remarks>
        /// <typeparam name="T">
        ///   The underling type of the <paramref name="source"/> <see
        ///   cref="None{T}"/> instance.
        /// </typeparam>
        /// <typeparam name="TResult">
        ///   The underlying type of the result <see cref="None{T}"/> instance.
        /// </typeparam>
        /// <param name="source">
        ///   The source <see cref="None{T}"/> instance.
        /// </param>
        /// <param name="selector">
        ///   A delegate that takes an argument of type <typeparamref name="T"/>
        ///   and returns an <code>None&lt;TResult&gt;</code> instance.
        /// </param>
        /// <returns>
        ///   An instance of type <code>None&lt;TResult&gt;</code> produced
        ///   by binding the <paramref name="source"/> argument with the
        ///   <paramref name="selector"/> delegate.
        /// </returns>
        public static None<TResult> Select<T, TResult>(
            this None<T> source,
            Func<T, None<TResult>> selector)
        {
            return source.Bind(selector);
        }

        /// <summary>
        ///   LINQ wrapper for the <see cref="None{T}.Bind{TResult}(Func{T,
        ///   None{TResult}})"/> method.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Unlike the other monads in this library, you can give a
        ///     <code>null</code> selector to <code>SelectMany(None{T},
        ///     ...)</code> as it is only evaluated for its side-effects.
        ///   </para>
        /// </remarks>
        /// <typeparam name="T">
        ///   The underlying type of the <paramref name="source"/> <see
        ///   cref="None{T}"/> instance.
        /// </typeparam>
        /// <typeparam name="TResult">
        ///   The underlying type of the result <see cref="None{T}"/> instance.
        /// </typeparam>
        /// <param name="source">
        ///   The source <see cref="None{T}"/> instance.
        /// </param>
        /// <param name="selector">
        ///   A delegate that takes an argument of type <typeparamref name="T"/>
        ///   and returns an <code>None&lt;TResult&gt;</code> instance.
        /// </param>
        /// <returns>
        ///   An instance of type <code>None&lt;TResult&gt;</code> produced
        ///   by binding the <paramref name="source"/> argument with the
        ///   <paramref name="selector"/> delegate.
        /// </returns>
        public static None<TResult> SelectMany<T, TResult>(
            this None<T> source,
            Func<T, None<TResult>> selector)
        {
            return Select(source, selector);
        }

        /// <summary>
        ///   LINQ wrapper for the <see cref="None{T}.Bind{TResult}(Func{T,
        ///   None{TResult}})"/> method used in nested "from..." queries.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Unlike the other monads in this library, you can give
        ///     <code>null</code> selectors to <code>SelectMany(None{T}, ...)</code>
        ///     as the selectors are only evaluated for their side-effects.
        ///   </para>
        /// </remarks>
        /// <typeparam name="T">
        ///   The right type of the <paramref name="source"/> <see
        ///   cref="None{T}"/> instance.
        /// </typeparam>
        /// <typeparam name="TInner">
        ///   The underlying type of the inner <see cref="None{T}"/> instance.
        /// </typeparam>
        /// <typeparam name="TResult">
        ///   The underlying type of the result <see cref="None{T}"/> instance.
        /// </typeparam>
        /// <param name="source">
        ///   The source <see cref="None{T}"/> instance.
        /// </param>
        /// <param name="innerSelector">
        ///   A delegate that takes an argument of type <typeparamref name="T"/>
        ///   and returns an <code>None&lt;TInner&gt;</code> instance.
        /// </param>
        /// <param name="resultSelector">
        ///   A delegate that takes arguments of type <typeparamref name="T"/>
        ///   and <typeparamref name="TInner"/>, and returns a value of type
        ///   <typeparamref name="TResult"/>.
        /// </param>
        /// <returns>
        ///   An instance of type <code>None&lt;TResult&gt;</code> produced
        ///   by nested binding of the <paramref name="source"/> argument with
        ///   the <paramref name="resultSelector"/> and <paramref
        ///   name="innerSelector"/> delegates.
        /// </returns>
        public static None<TResult> SelectMany<T, TInner, TResult>(
            this None<T> source,
            Func<T, None<TInner>> innerSelector,
            Func<T, TInner, TResult> resultSelector)
        {
            // The selectors are useless, but if they exist, we need to apply
            // them for their side-effects.
            if (!(innerSelector is null))
            {
                source.Bind(innerSelector);
            }
            if (!(resultSelector is null))
            {
                resultSelector(default(T), default(TInner));
            }

            return new None<TResult>();
        }
    }
}
