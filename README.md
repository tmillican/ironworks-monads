# IronWorks.Monads

## Project Description

__IronWorks.Monads__ is a collection of simple, CLS-compliant monads used by
the __IronWorks__ project.

## License

__IronWorks.Monads__ is released under the MIT License ([SPDX MIT][3]). A
[markdown version][4] of the license is provided in the repository.

[3]:https://spdx.org/licenses/MIT.html
[4]:LICENSE.md
