namespace IronWorks.Monads.Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using FluentAssertions;
    using Xunit;

    using IronWorks.Monads;

    /// <summary>
    ///   Tests for <see cref="EitherExtensions"/>.
    /// </summary>
    public static class EitherExtensionsTests
    {
        // static Either<L, R> ToEitherLeft<L, R>(this L value)
        // ---------------------------------------------------------------------

        [Fact]
        public static void ToEitherLeft_ShouldBeEquivalentTo_EitherLeft()
        {
            5.ToEitherLeft<int, int>().Should().BeEquivalentTo(
                Either<int, int>.Left(5));
        }

        // static Either<L, R> ToEitherRight<L, R>(this L value)
        // ---------------------------------------------------------------------

        [Fact]
        public static void ToEitherRight_ShouldBeEquivalentTo_EitherRight()
        {
            5.ToEitherRight<int, int>().Should().BeEquivalentTo(
                Either<int, int>.Right(5));
        }

        // static Either<L, RResult> SelectMany<L, R, RResult>(
        //     this Either<L, R> source,
        //     Func<R, Either<L, RResult>> selector)
        // ---------------------------------------------------------------------

        private static IEnumerable<object[]> _leftRightTable()
        {
            yield return new object[] { new Either<string, int>("hello", false) };
            yield return new object[] { new Either<string, int>(5) };
        }

        [Theory]
        [MemberData(nameof(_leftRightTable))]
        public static void
        TwoArgumentSelectMany_ThrowsArgumentNullException_WithNullSelector(
            Either<string, int> either)
        {
            Action selectMany =
                () => either.SelectMany<string, int, string>(null);

            selectMany.Should().Throw<ArgumentNullException>(
                "because we cannot apply a null function")
                .And.ParamName.Should().Be("selector");
        }

        private static IEnumerable<object[]> _twoArgumentSelectManyEquivalenceTable()
        {
            var monads = new Either<string, int>[] {
                new Either<string, int>("hello", false),
                new Either<string, int>(5),
            };

            var funcs = new Func<int, Either<string, double>>[] {
                i => new Either<string, double>("error1", false),
                i => new Either<string, double>((double)(i + 1)),
            };

            foreach (var m in monads)
            {
                foreach (var f in funcs)
                {
                    yield return new object[] { m, f };
                }
            }
        }

        [Theory]
        [MemberData(nameof(_twoArgumentSelectManyEquivalenceTable))]
        public static void TwoArgumentSelectMany_ShouldBeEquivalentToBind(
            Either<string, int> either, Func<int, Either<string, double>> f)
        {
            either.SelectMany(f)
                .Should().BeEquivalentTo(
                    either.Bind(f),
                    "because SelectMany is a LINQ-ism for monadic bind");
        }

        // public static Either<L, RResult> SelectMany<L, R, RInner, RResult>(
        //     this Either<L, R> source,
        //     Func<R, Either<L, RInner>> innerSelector,
        //     Func<R, RInner, RResult> resultSelector)
        // ---------------------------------------------------------------------

        [Theory]
        [MemberData(nameof(_leftRightTable))]
        public static void
        ThreeArgumentSelectMany_ThrowsArgumentNullException_WithNullInnerSelector(
            Either<string, int> either)
        {
            Func<int, double, string> resultSelector =
                (i, d) => (i + d).ToString();
            Action selectMany =
                () => either.SelectMany<string, int, double, string>(
                    null, resultSelector);

            selectMany.Should().Throw<ArgumentNullException>(
                "because we cannot apply a null function")
                .And.ParamName.Should().Be("innerSelector");
        }

        [Theory]
        [MemberData(nameof(_leftRightTable))]
        public static void
        ThreeArgumentSelectMany_ThrowsArgumentNullException_WithNullResultSelector(
            Either<string, int> either)
        {
            Func<int, Either<string, double>> innerSelector =
                i => new Either<string, double>((double)i + 1);
            Action selectMany =
                () => either.SelectMany<string, int, double, string>(
                    innerSelector, null);

            selectMany.Should().Throw<ArgumentNullException>(
                "because we cannot apply a null function")
                .And.ParamName.Should().Be("resultSelector");
        }

        private static IEnumerable<object[]> _threeArgumentSelectManyEquivalenceTable()
        {
            yield return new object[] {
                from x in 5.ToEitherRight<string, int>()
                from y in ((double)(x + 2)).ToEitherRight<string, double>()
                select (x + y).ToString(),

                ((double)(5 + 2)).ToString().ToEitherRight<string, string>()
            };

            yield return new object[] {
                from x in "error".ToEitherLeft<string, int>()
                from y in ((double)(x + 2)).ToEitherRight<string, double>()
                select (x + y).ToString(),

                "error".ToEitherLeft<string, string>()
            };

            yield return new object[] {
                from x in 5.ToEitherRight<string, int>()
                from y in $"error: {x}".ToEitherLeft<string, double>()
                select (x + y).ToString(),

                "error: 5".ToEitherLeft<string, string>()
            };
        }

        [Theory]
        public static void ThreeArgumentSelectMany_ShouldBeExpectedValue(
            Either<string, string> query, Either<string, string> expectedValue)
        {
            query.Should().BeEquivalentTo(
                expectedValue,
                "because SelectMany nests properly");
        }
    }
}
