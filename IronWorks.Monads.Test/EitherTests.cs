namespace IronWorks.Monads.Test
{
    using System;
    using System.Collections.Generic;

    using FluentAssertions;
    using Xunit;

    using IronWorks.Monads;

    /// <summary>
    ///   Tests of common <see cref="Either{L,R}"/> behavior.
    /// </summary>
    public static class EitherTests
    {
        // Left identity
        // ---------------------------------------------------------------------

        private static IEnumerable<object[]> _leftIdentityTable()
        {
            yield return new object[] {
                new Func<int, Either<string, double>>(
                    i => new Either<string, double>("error", false))
            };
            yield return new object[] {
                new Func<int, Either<string, double>>(
                    i => new Either<string, double>((double)(i + 1)))
            };
        }

        [Theory]
        [MemberData(nameof(_leftIdentityTable))]
        public static void LeftIdentity_ShouldHold(
            Func<int, Either<string, double>> f)
        {
            Either<string, int>.Identity(5).Bind(f)
                .Should().BeEquivalentTo(
                    f(5), "because left identity holds");
        }

        // Right identity
        // ---------------------------------------------------------------------

        private static IEnumerable<object[]> _rightIdentityTable()
        {
            yield return new object[] {
                new Either<string, int>("error", false)
            };
            yield return new object[] {
                new Either<string, int>(5)
            };
        }

        [Theory]
        [MemberData(nameof(_rightIdentityTable))]
        public static void RightIdentity_ShouldHold(
            Either<string, int> m)
        {
            m.Bind(Either<string, int>.Identity)
                .Should().BeEquivalentTo(
                    m, "because right identity holds");
        }

        // Associativity
        // ---------------------------------------------------------------------

        private static IEnumerable<object[]> _associativityTable()
        {
            var monads = new Either<string, int>[] {
                new Either<string, int>("hello", false),
                new Either<string, int>(5),
            };

            var fFuncs = new Func<int, Either<string, double>>[] {
                i => new Either<string, double>("error1", false),
                i => new Either<string, double>((double)(i + 1)),
            };

            var gFuncs = new Func<double, Either<string, int>>[] {
                d => new Either<string, int>("error2", false),
                d => new Either<string, int>((int)d + 1),
            };

            foreach (var m in monads)
            {
                foreach (var f in fFuncs)
                {
                    foreach (var g in gFuncs)
                    {
                        yield return new object[] { m, f, g };
                    }
                }
            }
        }

        [Theory]
        [MemberData(nameof(_associativityTable))]
        public static void Associativity_ShouldHold(
            Either<string, int> m,
            Func<int, Either<string, double>> f,
            Func<double, Either<string, int>> g)
        {
            m.Bind(f).Bind(g)
                .Should().BeEquivalentTo(
                    m.Bind(x => f(x).Bind(g)),
                    "because associativity holds");
        }

        // implicit Either()
        // ---------------------------------------------------------------------

        [Fact]
        public static void ImplicitCtor_ShouldBeEquivalentToDefaultLeft()
        {
            // ref L type
            new Either<string, string>().Should().BeEquivalentTo(
                new Either<string, string>(default(string), false));

            // value L type
            new Either<int, string>().Should().BeEquivalentTo(
                new Either<int, string>(default(int), false));
        }

        // Either(R)
        // ---------------------------------------------------------------------

        [Fact]
        public static void SingleArgumentCtor_ShouldBeEquivalentToIdentity()
        {
            new Either<string, int>(5).Should().BeEquivalentTo(
                Either<string, int>.Identity(5));
        }

        // Either(R)
        // ---------------------------------------------------------------------

        [Fact]
        public static void SingleArgumentCtor_ShouldBeEquivalentToRight()
        {
            new Either<string, int>(5).Should().BeEquivalentTo(
                Either<string, int>.Right(5));
        }

        // Either(L, bool)
        // ---------------------------------------------------------------------

        [Fact]
        public static void TwoArgumentCtor_ShouldBeEquivalentToLeft()
        {
            new Either<string, int>("foo", false).Should().BeEquivalentTo(
                Either<string, int>.Left("foo"));
        }

        // Either<L, RResult> Bind<RResult>(Func<R, RResult>)
        // ---------------------------------------------------------------------

        private static IEnumerable<object[]> _leftRightTable()
        {
            yield return new object[] { new Either<string, int>("hello", false) };
            yield return new object[] { new Either<string, int>(5) };
        }

        [Theory]
        [MemberData(nameof(_leftRightTable))]
        public static void Bind_ShouldThrowArgumentNullException_WithNullBindFunc(
            Either<string, int> either)
        {
            Action bind = () => either.Bind<string>(null);

            bind.Should().Throw<ArgumentNullException>(
                "because we cannot apply a null delegate")
                .And.ParamName.Should().Be(
                    "bindFunc",
                    "because we fully describe our exceptions");
        }

        [Fact]
        public static void Bind_ShouldLeftInject_WithLeftEither()
        {
            var hello = "hello";
            var either = new Either<string, int>(hello, false);
            var x = 0;
            Func<int, Either<string, int>> f =
                i => { x++; return new Either<string, int>(i + 1); };
            var result = either.Bind(f);

            result.IsLeft.Should().BeTrue(
                "because binding a left-sided Either produces another left-sided Either");

            result.Match(
                left: lv => lv,
                right: rv => "")
                .Should().Be(
                    hello,
                    "because binding a left-sided Either propagates its left value");

            x.Should().Be(
                0,
                "because Bind() does not evalutes 'bindFunc' with a left-sided Either.");
        }

        [Fact]
        public static void Bind_ShouldRightInject_WithRightEither()
        {
            var either = new Either<string, int>(5);
            Func<int, Either<string, int>> f =
                i => new Either<string, int>(i + 1);
            var result = either.Bind(f);

            result.IsRight.Should().BeTrue(
                "because binding a right Either produces another right Either");

            result.Match(
                left: lv => 0,
                right: rv => rv)
                .Should().Be(
                    6,
                    "because binding a right Either applies 'bindFunc' to its right value");
        }

        // bool Equals(Either<L, R> other)
        // ---------------------------------------------------------------------

        private static IEnumerable<object[]> _equalsEitherLRTable()
        {
            var foo = "foo";

            var fooEitherLeft = new Either<string, string>(foo, false);
            var nullEitherLeft = new Either<string, string>(null, false);

            var fooEitherRight = new Either<string, string>(foo);
            var nullEitherRight = new Either<string, string>(null);

            // "this" is Left sided Either with non-null value
            // -----------------------------------------------

            // other = self
            yield return new object[] {
                fooEitherLeft, fooEitherLeft, true
            };
            // other = equivalent, non-self left Either
            yield return new object[] {
                fooEitherLeft, new Either<string, string>(foo, false), true
            };
            // other = non-equivalent left Either
            yield return new object[] {
                fooEitherLeft, new Either<string, string>("bar", false), false
            };
            // other = right Either with coincidentally equivalent value
            yield return new object[] {
                fooEitherLeft, new Either<string, string>(foo), false
            };
            // other = right Either with non-equivalent internal value
            yield return new object[] {
                fooEitherLeft, new Either<string, string>("bar"), false
            };
            // other = left Either with null value
            yield return new object[] {
                fooEitherLeft,
                nullEitherLeft,
                false
            };
            // other = right Either with null value
            yield return new object[] {
                fooEitherLeft,
                nullEitherRight,
                false
            };

            // "this" is Left sided Either with null value
            // -------------------------------------------

            // other = self
            yield return new object[] {
                nullEitherLeft,
                nullEitherLeft,
                true
            };
            // other = equivalent, non-self left Either
            yield return new object[] {
                nullEitherLeft,
                new Either<string, string>(null, false),
                true
            };
            // other = non-equivalent left Either
            yield return new object[] {
                nullEitherLeft,
                fooEitherLeft,
                false
            };
            // other = right Either with coincidentally equivalent value
            yield return new object[] {
                nullEitherLeft,
                nullEitherRight,
                false
            };
            // other = right Either with non-equivalent internal value
            yield return new object[] {
                nullEitherLeft,
                fooEitherRight,
                false
            };

            // "this" is Right sided Either with non-null value
            // ------------------------------------------------

            // other = self
            yield return new object[] {
                fooEitherRight, fooEitherRight, true
            };
            // other = equivalent, non-self right Either
            yield return new object[] {
                fooEitherRight, new Either<string, string>(foo), true
            };
            // other = non-equivalent right Either
            yield return new object[] {
                fooEitherRight, new Either<string, string>("bar"), false
            };
            // other = left Either with coincidentally equivalent value
            yield return new object[] {
                fooEitherRight, new Either<string, string>(foo, false), false
            };
            // other = left Either with non-equivalent internal value
            yield return new object[] {
                fooEitherRight, new Either<string, string>("bar", false), false
            };
            // other = right Either with null value
            yield return new object[] {
                fooEitherRight,
                nullEitherRight,
                false
            };
            // other = left Either with null value
            yield return new object[] {
                fooEitherRight,
                nullEitherLeft,
                false
            };

            // "this" is Right sided Either with null value
            // --------------------------------------------

            // other = self
            yield return new object[] {
                nullEitherRight,
                nullEitherRight,
                true
            };
            // other = equivalent, non-self right Either
            yield return new object[] {
                nullEitherRight,
                new Either<string, string>(null),
                true
            };
            // other = non-equivalent right Either
            yield return new object[] {
                nullEitherRight,
                fooEitherRight,
                false
            };
            // other = left Either with coincidentally equivalent value
            yield return new object[] {
                nullEitherRight,
                nullEitherLeft,
                false
            };
            // other = left Either with non-equivalent internal value
            yield return new object[] {
                nullEitherRight,
                fooEitherLeft,
                false
            };
        }

        [Theory]
        [MemberData(nameof(_equalsEitherLRTable))]
        public static void EqualsEitherLR_ShouldBeExpectedValue(
            Either<string, string> either,
            Either<string, string> other,
            bool expectedValue)
        {
            either.Equals(other).Should().Be(expectedValue);
        }

        // bool Equals(object other)
        // ---------------------------------------------------------------------

        [Theory]
        [MemberData(nameof(_equalsEitherLRTable))]
        public static void EqualsObject_ShouldBeExpectedValue_WithEitherLRTable(
            Either<string, string> either,
            object other,
            bool expectedValue)
        {
            either.Equals(other).Should().Be(expectedValue);
        }

        private static IEnumerable<object[]> _equalsObjectWithNonEitherTable()
        {
            var foo = "foo";
            var fooEitherLeft = new Either<string, string>(foo, false);
            var nullEitherLeft = new Either<string, string>(null, false);

            var fooEitherRight = new Either<string, string>(foo);
            var nullEitherRight = new Either<string, string>(null);

            yield return new object[] {
                fooEitherLeft, new object()
            };
            yield return new object[] {
                nullEitherLeft, new object()
            };
            yield return new object[] {
                fooEitherRight, new object()
            };
            yield return new object[] {
                nullEitherRight, new object()
            };
            yield return new object[] {
                fooEitherLeft, null
            };
            yield return new object[] {
                nullEitherLeft, null
            };
            yield return new object[] {
                fooEitherRight, null
            };
            yield return new object[] {
                nullEitherRight, null
            };
        }

        [Theory]
        [MemberData(nameof(_equalsObjectWithNonEitherTable))]
        public static void EqualsObject_ShouldBeFalse_WithNonEitherOther(
            Either<string, string> either,
            object other)
        {
            either.Equals(other).Should().Be(false);
        }

        // bool EqualsLeft(R other)
        // ---------------------------------------------------------------------

        private static IEnumerable<object[]> _equalsLeftTable()
        {
            var foo = "foo";
            var fooEitherLeft = new Either<string, string>(foo, false);
            var nullEitherLeft = new Either<string, string>(null, false);

            var fooEitherRight = new Either<string, string>(foo);
            var nullEitherRight = new Either<string, string>(null);

            yield return new object[] {
                fooEitherLeft, foo, true
            };
            yield return new object[] {
                fooEitherLeft, "bar", false
            };
            yield return new object[] {
                fooEitherLeft, null, false
            };
            yield return new object[] {
                nullEitherLeft, foo, false
            };
            yield return new object[] {
                nullEitherLeft, null, true
            };

            yield return new object[] {
                fooEitherRight, foo, false
            };
            yield return new object[] {
                fooEitherRight, "bar", false
            };
            yield return new object[] {
                fooEitherRight, null, false
            };
            yield return new object[] {
                nullEitherRight, foo, false
            };
            yield return new object[] {
                nullEitherRight, null, false
            };
        }

        [Theory]
        [MemberData(nameof(_equalsLeftTable))]
        public static void EqualsLeft_ShouldBeExpectedValue(
            Either<string, string> either, string value, bool expectedValue)
        {
            either.EqualsLeft(value).Should().Be(expectedValue);
        }

        // bool EqualsRight(L other)
        // ---------------------------------------------------------------------

        private static IEnumerable<object[]> _equalsRightTable()
        {
            var foo = "foo";
            var fooEitherLeft = new Either<string, string>(foo, false);
            var nullEitherLeft = new Either<string, string>(null, false);

            var fooEitherRight = new Either<string, string>(foo);
            var nullEitherRight = new Either<string, string>(null);

            yield return new object[] {
                fooEitherLeft, foo, false
            };
            yield return new object[] {
                fooEitherLeft, "bar", false
            };
            yield return new object[] {
                fooEitherLeft, null, false
            };
            yield return new object[] {
                nullEitherLeft, foo, false
            };
            yield return new object[] {
                nullEitherLeft, null, false
            };

            yield return new object[] {
                fooEitherRight, foo, true
            };
            yield return new object[] {
                fooEitherRight, "bar", false
            };
            yield return new object[] {
                fooEitherRight, null, false
            };
            yield return new object[] {
                nullEitherRight, foo, false
            };
            yield return new object[] {
                nullEitherRight, null, true
            };
        }

        [Theory]
        [MemberData(nameof(_equalsRightTable))]
        public static void EqualsRight_ShouldBeExpectedValue(
            Either<string, string> either, string other, bool expectedValue)
        {
            either.EqualsRight(other).Should().Be(expectedValue);
        }

        // int GetHashCode()
        // ---------------------------------------------------------------------

        [Theory]
        [MemberData(nameof(_equalsEitherLRTable))]
        public static void GetHashCode_ShouldBeEqual_WithEquivalentEither(
            object either,
            object other,
            bool areEqual)
        {
            if (areEqual)
            {
                either.GetHashCode().Should().Be(
                    other.GetHashCode(),
                    "because equivalent things have equal hash codes");
            }
        }

        // bool IsLeft()
        // ---------------------------------------------------------------------

        private static IEnumerable<object[]> _isLeftTable()
        {
            yield return new object[] {
                new Either<string, int>("hello", false),
                true
            };
            yield return new object[] {
                new Either<string, int>(5),
                false
            };
        }

        [Theory]
        [MemberData(nameof(_isLeftTable))]
        public static void IsLeft_ShouldBeExpectedValue(
            Either<string, int> either, bool expectedValue)
        {
            either.IsLeft.Should().Be(expectedValue);
        }

        // bool IsRight()
        // ---------------------------------------------------------------------

        [Theory]
        [MemberData(nameof(_isLeftTable))]
        public static void IsRight_ShouldBeExpectedValue(
            Either<string, int> either, bool inverseExpectedValue)
        {
            either.IsRight.Should().Be(!inverseExpectedValue);
        }

        // TResult Match<TResult>(Func<L, TResult>, Func<R, TResult>)
        // ---------------------------------------------------------------------

        [Theory]
        [MemberData(nameof(_leftRightTable))]
        public static void Match_ShouldThrowArgumentNullException_WithNullLeft(
            Either<string, int> either)
        {
            Action match = () => either.Match<string>(null, x => "hello");

            match.Should().Throw<ArgumentNullException>(
                "because we cannot apply a null delegate")
                .And.ParamName.Should().Be(
                    "left",
                    "because we fully describe our exceptions");
        }

        [Theory]
        [MemberData(nameof(_leftRightTable))]
        public static void Match_ShouldThrowArgumentNullException_WithNullRight(
            Either<string, int> either)
        {
            Action match = () => either.Match<string>(x => x, null);

            match.Should().Throw<ArgumentNullException>(
                "because we cannot apply a null delegate")
                .And.ParamName.Should().Be(
                    "right",
                    "because we fully describe our exceptions");
        }

        private static IEnumerable<object[]> _matchTable()
        {
            var hello = "hello";
            yield return new object[] {
                new Either<string, int>(hello, false),
                new Func<string, string>(str => str),
                new Func<int, string>(i => ""),
                "hello"
            };
            yield return new object[] {
                new Either<string, int>(5),
                new Func<string, string>(str => ""),
                new Func<int, string>(i => $"{i}"),
                "5"
            };
        }

        [Theory]
        [MemberData(nameof(_matchTable))]
        public static void Match_ShouldBeExpectedValue(
            Either<string, int> either,
            Func<string, string> left,
            Func<int, string> right,
            string expectedValue)
        {
            either.Match(left, right).Should().Be(expectedValue);
        }

        // static Either<L,R> Right(R)
        // ---------------------------------------------------------------------

        [Fact]
        public static void Right_ShouldBeEquivalentToIdentity()
        {
            Either<string, int>.Right(5).Should().BeEquivalentTo(
                Either<string, int>.Identity(5));
        }

        // string ToString()
        // ---------------------------------------------------------------------

        private static IEnumerable<object[]> _toStringTable()
        {
            yield return new object[] {
                new Either<string, string>("foo", false), "foo"
            };
            yield return new object[] {
                new Either<string, string>(null, false), ""
            };
            yield return new object[] {
                new Either<string, string>("foo"), "foo"
            };
            yield return new object[] {
                new Either<string, string>(null), ""
            };
        }

        [Theory]
        [MemberData(nameof(_toStringTable))]
        public static void ToString_ShouldBeExpectedValue(
            Either<string, string> either, string expectedValue)
        {
            either.ToString().Should().Be(expectedValue);
        }
    }
}
