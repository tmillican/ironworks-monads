namespace IronWorks.Monads.Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using FluentAssertions;
    using Xunit;

    using IronWorks.Monads;

    /// <summary>
    ///   Tests for <see cref="NoneExtensions"/>.
    /// </summary>
    public static class NoneExtensionsTests
    {
        // static None<RResult> SelectMany<T, TResult>(
        //     this None<T> source,
        //     Func<R, None<T, TResult>> selector)
        // ---------------------------------------------------------------------

        [Fact]
        public static void TwoArgumentSelectMany_ShouldBeEquivalentToBind()
        {
            var none = new None<int>();
            Func<int, None<string>> f =
                i => new None<string>();

            none.SelectMany(f)
                .Should().BeEquivalentTo(
                    none.Bind(f),
                    "because SelectMany is a LINQ-ism for monadic bind");
        }

        // static None<TResult> SelectMany<T, TInner, TResult>(
        //     this None<T> source,
        //     Func<T, None<TInner>> innerSelector,
        //     Func<T, TInner, TResult> resultSelector)
        // ---------------------------------------------------------------------

        [Theory]
        public static void ThreeArgumentSelectMany_ShouldBeExpectedValue()
        {
            var i = 0;
            var j = 0;
            (from x in new None<int>()
             from y in new None<double>((double)(i++))
             select $"{x}, {y}, {j++}")
            .Should().BeEquivalentTo(
                new None<string>(),
                "because SelectMany nests properly");

            i.Should().Be(3, "because SelectMany evaluates outerSelector for side-effects");

            j.Should().Be(4, "because SelectMany evaluates innerSelector for side-effects");
        }
    }
}
