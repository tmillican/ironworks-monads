namespace IronWorks.Monads.Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using FluentAssertions;
    using Xunit;

    using IronWorks.Monads;

    /// <summary>
    ///   Tests for <see cref="OptionExtensions"/>.
    /// </summary>
    public static class OptionExtensionsTests
    {
        // static Option<T> ToOptionNone<T>(this T value)
        // ---------------------------------------------------------------------

        [Fact]
        public static void ToOptionNone_ShouldBeEquivalentTo_OptionNone()
        {
            5.ToOptionNone<int>().Should().BeEquivalentTo(
                Option<int>.None());
        }

        // static Option<T> ToOptionSome<T>(this T value)
        // ---------------------------------------------------------------------

        [Fact]
        public static void ToOptionSome_ShouldBeEquivalentTo_OptionSome()
        {
            5.ToOptionSome<int>().Should().BeEquivalentTo(
                Option<int>.Some(5));
        }

        // static Option<TResult> SelectMany<T, TResult>(
        //     this Option<T> source,
        //     Func<T, Option<TResult>> selector)
        // ---------------------------------------------------------------------

        private static IEnumerable<object[]> _noneSomeTable()
        {
            yield return new object[] { new Option<int>(false, false) };
            yield return new object[] { new Option<int>(5) };
        }

        [Theory]
        [MemberData(nameof(_noneSomeTable))]
        public static void
        TwoArgumentSelectMany_ThrowsArgumentNullException_WithNullSelector(
            Option<int> option)
        {
            Action selectMany =
                () => option.SelectMany<int, string>(null);

            selectMany.Should().Throw<ArgumentNullException>(
                "because we cannot apply a null function")
                .And.ParamName.Should().Be("selector");
        }

        private static IEnumerable<object[]> _twoArgumentSelectManyEquivalenceTable()
        {
            var monads = new Option<int>[] {
                new Option<int>(false, false),
                new Option<int>(5),
            };

            var funcs = new Func<int, Option<double>>[] {
                i => new Option<double>(false, false),
                i => new Option<double>((double)(i + 1)),
            };

            foreach (var m in monads)
            {
                foreach (var f in funcs)
                {
                    yield return new object[] { m, f };
                }
            }
        }

        [Theory]
        [MemberData(nameof(_twoArgumentSelectManyEquivalenceTable))]
        public static void TwoArgumentSelectMany_ShouldBeEquivalentToBind(
            Option<int> option, Func<int, Option<double>> f)
        {
            option.SelectMany(f)
                .Should().BeEquivalentTo(
                    option.Bind(f),
                    "because SelectMany is a LINQ-ism for monadic bind");
        }

        // public static Option<TResult> SelectMany<T, TInner, TResult>(
        //     this Option<T> source,
        //     Func<T, Option<TInner>> innerSelector,
        //     Func<T, TInner, TResult> resultSelector)
        // ---------------------------------------------------------------------

        [Theory]
        [MemberData(nameof(_noneSomeTable))]
        public static void
        ThreeArgumentSelectMany_ThrowsArgumentNullException_WithNullInnerSelector(
            Option<int> option)
        {
            Func<int, double, string> resultSelector =
                (i, d) => (i + d).ToString();
            Action selectMany =
                () => option.SelectMany<int, double, string>(
                    null, resultSelector);

            selectMany.Should().Throw<ArgumentNullException>(
                "because we cannot apply a null function")
                .And.ParamName.Should().Be("innerSelector");
        }

        [Theory]
        [MemberData(nameof(_noneSomeTable))]
        public static void
        ThreeArgumentSelectMany_ThrowsArgumentNullException_WithNullResultSelector(
            Option<int> option)
        {
            Func<int, Option<double>> innerSelector =
                i => new Option<double>((double)i + 1);
            Action selectMany =
                () => option.SelectMany<int, double, string>(
                    innerSelector, null);

            selectMany.Should().Throw<ArgumentNullException>(
                "because we cannot apply a null function")
                .And.ParamName.Should().Be("resultSelector");
        }

        private static IEnumerable<object[]> _threeArgumentSelectManyEquivalenceTable()
        {
            yield return new object[] {
                from x in 5.ToOptionSome<int>()
                from y in ((double)(x + 2)).ToOptionSome<double>()
                select (x + y).ToString(),

                ((double)(5 + 2)).ToString().ToOptionSome<string>()
            };

            yield return new object[] {
                from x in 5.ToOptionNone<int>()
                from y in ((double)(x + 2)).ToOptionSome<double>()
                select (x + y).ToString(),

                "error".ToOptionNone<string>()
            };

            yield return new object[] {
                from x in 5.ToOptionSome<int>()
                from y in (5.0).ToOptionNone<double>()
                select (x + y).ToString(),

                "error: 5".ToOptionNone<string>()
            };
        }

        [Theory]
        public static void ThreeArgumentSelectMany_ShouldBeExpectedValue(
            Option<string> query, Option<string> expectedValue)
        {
            query.Should().BeEquivalentTo(
                expectedValue,
                "because SelectMany nests properly");
        }
    }
}
