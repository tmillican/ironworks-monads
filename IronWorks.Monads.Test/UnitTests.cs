namespace IronWorks.Monads.Test
{
    using FluentAssertions;
    using Xunit;

    using IronWorks.Monads;

    /// <summary>
    ///   Tests for <see cref="Unit"/>.
    /// </summary>
    /// <remarks>
    ///   <para>
    ///     In short, UnitTests are the units tests for the Unit unit. I don't
    ///     see what's so confusing about that.
    ///   </para>
    /// </remarks>
    public static class UnitTests
    {
        // bool Equals(Unit)
        // ---------------------------------------------------------------------

        [Fact]
        public static void EqualsUnit_ShouldBeTrue_WithNewUnit()
        {
            Unit unit = new Unit();
            Unit other = new Unit();
            unit.Equals(other).Should().BeTrue(
                "because all Unit instances have the same value");
        }

        [Fact]
        public static void EqualsUnit_ShouldBeTrue_WithDefaultUnit()
        {
            Unit unit = new Unit();
            Unit[] otherArray = new Unit[1];
            unit.Equals(otherArray[0]).Should().BeTrue(
                "because all Unit instances have the same value");
        }

        // bool Equals(object)
        // ---------------------------------------------------------------------

        [Fact]
        public static void EqualsObject_ShouldBeTrue_WithOtherUnit()
        {
            Unit unit = new Unit();
            object other = new Unit();
            unit.Equals(other).Should().BeTrue(
                "because all Unit instances have the same value");
        }

        [Fact]
        public static void EqualsObject_ShouldBeFalse_WithNonUnit()
        {
            Unit unit = new Unit();
            object other = "hello";
            unit.Equals(other).Should().BeFalse(
                "because non-Unit objects are not equatable to Unit");
        }

        // This is actually covered by EqualsObject_ShouldBeFalse_WithNonUnit(), but I
        // want an explicit test for this point of semantics.
        [Fact]
        public static void EqualsObject_ShouldBeFalse_WithNull()
        {
            Unit unit = new Unit();
            object other = null;
            unit.Equals(other).Should().BeFalse(
                "because null cannot inhabit Unit");
        }
    }
}
