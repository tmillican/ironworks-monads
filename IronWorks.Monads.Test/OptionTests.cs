namespace IronWorks.Monads.Test
{
    using System;
    using System.Collections.Generic;

    using FluentAssertions;
    using Xunit;

    using IronWorks.Monads;

    /// <summary>
    ///   Tests for <see cref="Option{T}"/>.
    /// </summary>
    public static class OptionTests
    {
        // Left identity
        // ---------------------------------------------------------------------

        private static IEnumerable<object[]> _leftIdentityTable()
        {
            yield return new object[] {
                new Func<int, Option<string>>(
                    i => new Option<string>())
            };
            yield return new object[] {
                new Func<int, Option<string>>(
                    i => new Option<string>($"{i}"))
            };
        }

        [Theory]
        [MemberData(nameof(_leftIdentityTable))]
        public static void LeftIdentity_ShouldHold(
            Func<int, Option<string>> f)
        {
            Option<int>.Identity(5).Bind(f)
                .Should().BeEquivalentTo(
                    f(5), "because left identity holds");
        }

        // Right identity
        // ---------------------------------------------------------------------

        private static IEnumerable<object[]> _rightIdentityTable()
        {
            yield return new object[] {
                new Option<string>()
            };
            yield return new object[] {
                new Option<string>("foo")
            };
        }

        [Theory]
        [MemberData(nameof(_rightIdentityTable))]
        public static void RightIdentity_ShouldHold(
            Option<string> m)
        {
            m.Bind(Option<string>.Identity)
                .Should().BeEquivalentTo(
                    m, "because right identity holds");
        }

        // Associativity
        // ---------------------------------------------------------------------

        private static IEnumerable<object[]> _associativityTable()
        {
            var monads = new Option<int>[] {
                new Option<int>(),
                new Option<int>(5),
            };

            var fFuncs = new Func<int, Option<double>>[] {
                i => new Option<double>(),
                i => new Option<double>((double)i + 1),
            };

            var gFuncs = new Func<double, Option<string>>[] {
                d => new Option<string>(),
                d => new Option<string>($"{d + 1}"),
            };

            foreach (var m in monads)
            {
                foreach (var f in fFuncs)
                {
                    foreach (var g in gFuncs)
                    {
                        yield return new object[] { m, f, g };
                    }
                }
            }
        }

        [Theory]
        [MemberData(nameof(_associativityTable))]
        public static void Associativity_ShouldHold(
            Option<int> m,
            Func<int, Option<double>> f,
            Func<double, Option<string>> g)
        {
            m.Bind(f).Bind(g)
                .Should().BeEquivalentTo(
                    m.Bind(x => f(x).Bind(g)),
                    "because associativity holds");
        }

        // implicit Option()
        // ---------------------------------------------------------------------

        [Fact]
        public static void ImplicitCtor_ShouldBeEquivalentToNone()
        {
            // ref T type
            new Option<string>().Should().BeEquivalentTo(
                Option<string>.None());

            // value T type
            new Option<int>().Should().BeEquivalentTo(
                Option<int>.None());
        }

        // Option(T)
        // ---------------------------------------------------------------------

        [Fact]
        public static void SingleArgumentCtor_ShouldBeEquivalentToIdentity()
        {
            new Option<int>(5).Should().BeEquivalentTo(
                Option<int>.Identity(5));
        }

        // Option(T)
        // ---------------------------------------------------------------------

        [Fact]
        public static void SingleArgumentCtor_ShouldBeEquivalentToSome()
        {
            new Option<int>(5).Should().BeEquivalentTo(
                Option<int>.Some(5));
        }

        // Option(bool, bool)
        // ---------------------------------------------------------------------

        [Fact]
        public static void TwoArgumentCtor_ShouldBeEquivalentToNone()
        {
            new Option<int>(false, false).Should().BeEquivalentTo(
                Option<int>.None());
        }

        // Option<TResult> Bind<TResult>(Func<T, TResult>)
        // ---------------------------------------------------------------------

        private static IEnumerable<object[]> _noneSomeTable()
        {
            yield return new object[] { new Option<string>() };
            yield return new object[] { new Option<string>("hello") };
        }

        [Theory]
        [MemberData(nameof(_noneSomeTable))]
        public static void Bind_ShouldThrowArgumentNullException_WithNullBindFunc(
            Option<string> option)
        {
            Action bind = () => option.Bind<string>(null);

            bind.Should().Throw<ArgumentNullException>(
                "because we cannot apply a null delegate")
                .And.ParamName.Should().Be(
                    "bindFunc",
                    "because we fully describe our exceptions");
        }

        [Fact]
        public static void Bind_ShouldNoneInject_WithNoneOption()
        {
            var option = new Option<int>();
            var x = 0;
            Func<int, Option<int>> f =
                i => { x++; return new Option<int>(i + 1); };
            var result = option.Bind(f);

            result.IsNone.Should().BeTrue(
                "because binding a none Option produces another none Option");

            result.Match(
                none: lv => lv.Equals(new None<int>()),
                some: rv => false)
                .Should().BeTrue(
                    "because binding a none Option propagates its none value");

            x.Should().Be(
                0,
                "because Bind() does not evalutes 'bindFunc' with a none Option.");
        }

        [Fact]
        public static void Bind_ShouldSomeInject_WithSomeOption()
        {
            var option = new Option<int>(5);
            Func<int, Option<int>> f =
                i => new Option<int>(i + 1);
            var result = option.Bind(f);

            result.IsSome.Should().BeTrue(
                "because binding a some Option produces another some Option");

            result.Match(
                none: lv => false,
                some: rv => rv == 6)
                .Should().BeTrue(
                    "because binding a some Option applies 'bindFunc' to its value");
        }

        // bool Equals(Option<L> other)
        // ---------------------------------------------------------------------

        private static IEnumerable<object[]> _equalsOptionTTable()
        {
            var foo = "foo";

            var noneOption = new Option<string>(false, false);

            var someFooOption = new Option<string>(foo);
            var someNullOption = new Option<string>(null);

            // "this" is none Option
            // ---------------------

            // other = self
            yield return new object[] {
                noneOption, noneOption, true
            };
            // other = non-self none Option
            yield return new object[] {
                noneOption, new Option<string>(false, false), true
            };
            // other = some Option with non-null value
            yield return new object[] {
                noneOption, someFooOption, false
            };
            // other = some Option with null value
            yield return new object[] {
                noneOption, someNullOption, false
            };

            // "this" is some Option with non-null internal value
            // --------------------------------------------------

            // other = self
            yield return new object[] {
                someFooOption, someFooOption, true
            };
            // other = equivalent, non-self some Option
            yield return new object[] {
                someFooOption, new Option<string>(foo), true
            };
            // other = non-equivalent some Option
            yield return new object[] {
                someFooOption, new Option<string>("bar"), false
            };
            // other = right Option with null value
            yield return new object[] {
                someFooOption,
                someNullOption,
                false
            };
            // other = none Option
            yield return new object[] {
                someFooOption, noneOption, false
            };

            // "this" is some Option with null internal value
            // ----------------------------------------------

            // other = self
            yield return new object[] {
                someNullOption,
                someNullOption,
                true
            };
            // other = equivalent, non-self some Option
            yield return new object[] {
                someNullOption,
                new Option<string>(null),
                true
            };
            // other = non-equivalent right Option
            yield return new object[] {
                someNullOption,
                someFooOption,
                false
            };
            // other = none Option
            yield return new object[] {
                someNullOption,
                noneOption,
                false
            };
        }

        [Theory]
        [MemberData(nameof(_equalsOptionTTable))]
        public static void EqualsOptionT_ShouldBeExpectedValue(
            Option<string> option,
            Option<string> other,
            bool expectedValue)
        {
            option.Equals(other).Should().Be(expectedValue);
        }

        // bool Equals(object other)
        // ---------------------------------------------------------------------

        [Theory]
        [MemberData(nameof(_equalsOptionTTable))]
        public static void EqualsObject_ShouldBeExpectedValue(
            Option<string> option,
            object other,
            bool expectedValue)
        {
            option.Equals(other).Should().Be(expectedValue);
        }

        private static IEnumerable<object[]> _equalsObjectWithNonOptionTable()
        {
            var foo = "foo";
            var noneOption = new Option<string>(false, false);
            var someFooOption = new Option<string>(foo);
            var someNullOption = new Option<string>(null);

            yield return new object[] {
                noneOption, new object()
            };
            yield return new object[] {
                someFooOption, new object()
            };
            yield return new object[] {
                someNullOption, new object()
            };
            yield return new object[] {
                noneOption, null
            };
            yield return new object[] {
                someFooOption, null
            };
            yield return new object[] {
                someNullOption, null
            };
        }

        [Theory]
        [MemberData(nameof(_equalsObjectWithNonOptionTable))]
        public static void EqualsObject_ShouldBeFalse_WithNonOptionOther(
            Option<string> option,
            object other)
        {
            option.Equals(other).Should().Be(false);
        }

        // bool EqualsNone(None<T> other)
        // ---------------------------------------------------------------------

        private static IEnumerable<object[]> _equalsNoneTable()
        {
            var foo = "foo";
            var noneOption = new Option<string>(false, false);
            var someFooOption = new Option<string>(foo);
            var someNullOption = new Option<string>(null);

            yield return new object[] {
                noneOption, true
            };
            yield return new object[] {
                someFooOption, false
            };
            yield return new object[] {
                someNullOption, false
            };
        }

        [Theory]
        [MemberData(nameof(_equalsNoneTable))]
        public static void EqualsNone_ShouldBeExpectedValue(
            Option<string> option, bool expectedValue)
        {
            option.EqualsNone(new None<string>()).Should().Be(expectedValue);
        }

        // bool EqualsSome(T other)
        // ---------------------------------------------------------------------

        private static IEnumerable<object[]> _equalsSomeTable()
        {
            var foo = "foo";
            var noneOption = new Option<string>(false, false);
            var someFooOption = new Option<string>(foo);
            var someNullOption = new Option<string>(null);

            yield return new object[] {
                noneOption, null, false
            };
            yield return new object[] {
                noneOption, foo, false
            };

            yield return new object[] {
                someFooOption, foo, true
            };
            yield return new object[] {
                someFooOption, "bar", false
            };
            yield return new object[] {
                someFooOption, null, false
            };
            yield return new object[] {
                someNullOption, foo, false
            };
            yield return new object[] {
                someNullOption, null, true
            };
        }

        [Theory]
        [MemberData(nameof(_equalsSomeTable))]
        public static void EqualsSome_ShouldBeExpectedValue(
            Option<string> option, string other, bool expectedValue)
        {
            option.EqualsSome(other).Should().Be(expectedValue);
        }

        // int GetHashCode()
        // ---------------------------------------------------------------------

        [Theory]
        [MemberData(nameof(_equalsOptionTTable))]
        public static void GetHashCode_ShouldBeEqual_WithEquivalentOption(
            object option,
            object other,
            bool areEqual)
        {
            if (areEqual)
            {
                option.GetHashCode().Should().Be(
                    other.GetHashCode(),
                    "because equivalent things have equal hash codes");
            }
        }

        // bool IsLeft()
        // ---------------------------------------------------------------------

        private static IEnumerable<object[]> _isNoneTable()
        {
            yield return new object[] {
                new Option<string>(false, false),
                true
            };
            yield return new object[] {
                new Option<string>("foo"),
                false
            };
        }

        [Theory]
        [MemberData(nameof(_isNoneTable))]
        public static void IsNone_ShouldBeExpectedValue(
            Option<string> option, bool expectedValue)
        {
            option.IsNone.Should().Be(expectedValue);
        }

        // bool IsSome()
        // ---------------------------------------------------------------------

        [Theory]
        [MemberData(nameof(_isNoneTable))]
        public static void IsSome_ShouldBeExpectedValue(
            Option<string> option, bool inverseExpectedValue)
        {
            option.IsSome.Should().Be(!inverseExpectedValue);
        }

        // TResult Match<TResult>(Func<None<T>, TResult>, Func<T, TResult>)
        // ---------------------------------------------------------------------

        [Theory]
        [MemberData(nameof(_noneSomeTable))]
        public static void Match_ShouldThrowArgumentNullException_WithNullNone(
            Option<string> option)
        {
            Action match = () => option.Match<string>(null, x => "hello");

            match.Should().Throw<ArgumentNullException>(
                "because we cannot apply a null delegate")
                .And.ParamName.Should().Be(
                    "none",
                    "because we fully describe our exceptions");
        }

        [Theory]
        [MemberData(nameof(_noneSomeTable))]
        public static void Match_ShouldThrowArgumentNullException_WithNullSome(
            Option<string> option)
        {
            Action match = () => option.Match<string>(x => "", null);

            match.Should().Throw<ArgumentNullException>(
                "because we cannot apply a null delegate")
                .And.ParamName.Should().Be(
                    "some",
                    "because we fully describe our exceptions");
        }

        private static IEnumerable<object[]> _matchTable()
        {
            yield return new object[] {
                new Option<int>(false, false),
                new Func<None<int>, bool>(n => true),
                new Func<int, bool>(i => false),
                true
            };
            yield return new object[] {
                new Option<int>(5),
                new Func<None<int>, bool>(n => false),
                new Func<int, bool>(i => true),
                true
            };
        }

        [Theory]
        [MemberData(nameof(_matchTable))]
        public static void Match_ShouldBeExpectedValue(
            Option<int> option,
            Func<None<int>, bool> left,
            Func<int, bool> right,
            bool expectedValue)
        {
            option.Match(left, right).Should().Be(expectedValue);
        }

        // static Option<T> Some(T)
        // ---------------------------------------------------------------------

        [Fact]
        public static void Some_ShouldBeEquivalentToIdentity()
        {
            Option<int>.Some(5).Should().BeEquivalentTo(
                Option<int>.Identity(5));
        }

        // string ToString()
        // ---------------------------------------------------------------------

        private static IEnumerable<object[]> _toStringTable()
        {
            yield return new object[] {
                new Option<string>(false, false), "None<System.String>"
            };
            yield return new object[] {
                new Option<string>("foo"), "foo"
            };
            yield return new object[] {
                new Option<string>(null), ""
            };
        }

        [Theory]
        [MemberData(nameof(_toStringTable))]
        public static void ToString_ShouldBeExpectedValue(
            Option<string> option, string expectedValue)
        {
            option.ToString().Should().Be(expectedValue);
        }
    }
}
