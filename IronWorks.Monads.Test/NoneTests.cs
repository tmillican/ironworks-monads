namespace IronWorks.Monads.Test
{
    using System;
    using System.Collections.Generic;

    using FluentAssertions;
    using Xunit;

    using IronWorks.Monads;

    /// <summary>
    ///   Tests of common <see cref="Either{L,R}"/> behavior.
    /// </summary>
    public static class NoneTests
    {
        // None(T)
        // ---------------------------------------------------------------------

        [Fact]
        public static void SingleArgumentCtor_ShouldBeEquivalentToZeroArgumentCtor()
        {
            new None<int>(5).Should().BeEquivalentTo(
                new None<int>(),
                "because None is a unit value");
        }

        [Fact]
        public static void SingleArgumentCtor_ShouldIgnoreValue()
        {
            new None<int>(5).Should().BeEquivalentTo(
                new None<int>(7),
                "because None is a unit value");
        }

        // Left identity
        // ---------------------------------------------------------------------

        [Fact]
        public static void LeftIdentity_ShouldHold()
        {
            Func<int, None<string>> f =
                i => new None<string>();
            None<int>.Identity(5).Bind(f)
                .Should().BeEquivalentTo(
                    f(5), "because left identity holds");
        }

        // Right identity
        // ---------------------------------------------------------------------

        [Fact]
        public static void RightIdentity_ShouldHold()
        {
            var m = None<int>.Identity(5);
            m.Bind(None<int>.Identity)
                .Should().BeEquivalentTo(
                    m, "because right identity holds");
        }

        // Associativity
        // ---------------------------------------------------------------------

        [Fact]
        public static void Associativity_ShouldHold()
        {
            var m = None<int>.Identity(5);
            Func<int, None<double>> f =
                i => new None<double>();
            Func<double, None<string>> g =
                d => new None<string>();

            m.Bind(f).Bind(g)
                .Should().BeEquivalentTo(
                    m.Bind(x => f(x).Bind(g)),
                    "because associativity holds");
        }

        // Either<L, RResult> Bind<RResult>(Func<R, RResult>)
        // ---------------------------------------------------------------------

        [Fact]
        public static void Bind_ShouldBeExpectedNone()
        {
            new None<int>().Bind<double>(x => new None<double>())
                .Should().BeEquivalentTo(new None<double>());
        }

        // bool Equals(None<T> other)
        // ---------------------------------------------------------------------

        [Fact]
        public static void EqualsNoneT_ShouldBeTrue()
        {
            new None<int>(5).Equals(new None<int>(7))
                .Should().BeTrue("for a given T, None<T> is a unit value");
        }

        // override bool Equals(object other)
        // ---------------------------------------------------------------------

        private static IEnumerable<object[]> _equalsObjectTable()
        {
            var none = new None<int>();
            yield return new object[] {
                none, none, true
            };
            yield return new object[] {
                none, new None<int>(), true
            };
            yield return new object[] {
                none, new None<double>(), false
            };
            yield return new object[] {
                none, null, false
            };
        }

        [Theory]
        [MemberData(nameof(_equalsObjectTable))]
        public static void EqualsObjet_ShouldBeExpectedValue(
            None<int> none, object other, bool expectedValue)
        {
            none.Equals(other).Should().Be(expectedValue);
        }

        // override int GetHashCode()
        // ---------------------------------------------------------------------

        [Fact]
        public static void GetHashCode_ShouldBeSame_ForAllNoneT()
        {
            object noneA = new None<int>(3);
            object noneB = new None<int>(5);
            noneA.GetHashCode().Should().Be(
                noneB.GetHashCode(),
                "for a given T, all None<T>'s have the same hashcode");
        }

        // static None<T> Identity(T value)
        // ---------------------------------------------------------------------

        [Fact]
        public static void Identity_ShouldIgnoreValue()
        {
            None<int>.Identity(5).Should().BeEquivalentTo(
                None<int>.Identity(7),
                "because None is a unit value");
        }

        [Fact]
        public static void Identity_ShouldBeEquivalentToCtors()
        {
            None<int>.Identity(5).Should().BeEquivalentTo(
                new None<int>(),
                "because Identity is equivalent to implicit default ctor");

            None<int>.Identity(5).Should().BeEquivalentTo(
                new None<int>(7),
                "because Identity is equivalent to single argument ctor");
        }

        // override string ToString()
        // ---------------------------------------------------------------------

        [Fact]
        public static void ToString_ShouldBeExpectedValue()
        {
            object none = new None<int>();
            none.ToString().Should().Be("None<System.Int32>");
        }
    }
}
